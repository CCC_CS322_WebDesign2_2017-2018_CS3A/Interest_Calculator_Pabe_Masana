var selectedHigh = document.getElementById('compound');
var selectedHigh2 = document.getElementById('period2');
var btnReset = document.getElementById('reset');
var calculate = document.getElementById('submit');
var results = document.getElementById('result-comment');
var interestDisplay = document.getElementById('total-interest');

var calc = document.getElementById('submit');
calc.onclick = calculateInterest;
btnReset.onclick = resetParas;

$('.button-line').click(function () {
    $(this).parent('.content').fadeOut(1000);
	$('.image-holder .bubbles').delay(2000).fadeIn(1000).delay(4000).fadeOut(1000);
});

function resetParas() {
	results.style.opacity = "0";
	interestDisplay.style.opacity = "0";
}


function validCheck() {
	var principal = document.forms["calcForm"]["principal"].value;
	var rate = document.forms["calcForm"]["rate"].value;
	var period1 = document.forms["calcForm"]["period1"].value;
	
	
	
	if(principal == "" || rate == "" || period1 == "" || selectedHigh.selectedIndex == 0 || selectedHigh2.selectedIndex == 0){
		calculate.style.opacity = "0.5";
		calculate.disabled = true;
		calculate.style.cursor = "context-menu";
	}
	else if(principal != "" && rate != "" && period1 != "" && selectedHigh.selectedIndex != 0 && selectedHigh2.selectedIndex != 0){
		calculate.style.opacity = "1";
		calculate.disabled = false;
		calculate.style.cursor = "pointer";
	}
	
	
	setTimeout(validCheck, 1000);
}

validCheck();

function highlight() {
	

	
	if(selectedHigh.selectedIndex == 0){
		selectedHigh.style.opacity = "0.5";
	}
	else if(selectedHigh.selectedIndex == 1){
		selectedHigh.style.opacity = "1";
	}
	else if(selectedHigh.selectedIndex == 2){
		selectedHigh.style.opacity = "1";
	}
	else if(selectedHigh.selectedIndex == 3){
		selectedHigh.style.opacity = "1";
	}
	
	if(selectedHigh2.selectedIndex == 0){
		selectedHigh2.style.opacity = "0.5";
	}
	else if(selectedHigh2.selectedIndex == 1){
		selectedHigh2.style.opacity = "1";
	}
	else if(selectedHigh2.selectedIndex == 2){
		selectedHigh2.style.opacity = "1";
	}
	else if(selectedHigh2.selectedIndex == 3){
		selectedHigh2.style.opacity = "1";
	}
	
	setTimeout(highlight, 1000);
}

highlight();

function calculateInterest() {
	var prin = document.getElementById('principal').value;
	var rate = document.getElementById('rate').value;
	var period1 = document.getElementById('period1').value;
	var per = selectedHigh.options[selectedHigh.selectedIndex].text;
	var after = selectedHigh2.options[selectedHigh2.selectedIndex].text;
	var calculatedRate, totalInterest;
	
	if(selectedHigh.selectedIndex == 1)
	{
		if(selectedHigh2.selectedIndex == 1)
		{
			calculatedRate = (rate/1)/100;
			totalInterest = prin * calculatedRate * period1;
		}
		else if(selectedHigh2.selectedIndex == 2)
		{
			calculatedRate = (rate/12)/100;
			totalInterest = prin * calculatedRate * period1;
		}
		else if(selectedHigh2.selectedIndex == 3)
		{
			calculatedRate = (rate/3)/100;
			totalInterest = prin * calculatedRate * period1;
		}
	}
	else if(selectedHigh.selectedIndex == 2)
	{
		if(selectedHigh2.selectedIndex == 1)
		{
			calculatedRate = (rate*12)/100;
			totalInterest = prin * calculatedRate * period1;
		}
		else if(selectedHigh2.selectedIndex == 2)
		{
			calculatedRate = (rate*1)/100;
			totalInterest = prin * calculatedRate * period1;
		}
		else if(selectedHigh2.selectedIndex == 3)
		{
			calculatedRate = (rate*3)/100;
			totalInterest = prin * calculatedRate * period1;
		}
	}
	else if(selectedHigh.selectedIndex == 3)
	{
		if(selectedHigh2.selectedIndex == 1)
		{
			calculatedRate = (rate*4)/100;
			totalInterest = prin * calculatedRate * period1;
		}
		else if(selectedHigh2.selectedIndex == 2)
		{
			calculatedRate = (rate/3)/100;
			totalInterest = prin * calculatedRate * period1;
		}
		else if(selectedHigh2.selectedIndex == 3)
		{
			calculatedRate = (rate/1)/100;
			totalInterest = prin * calculatedRate * period1;
		}
	}
	document.getElementById('result-comment').innerHTML = "You want to calculate the interest on $" + prin + " at " + rate + "% interest per " + per + " after "+ period1 + " " + after + ".";
	document.getElementById('total-interest').innerHTML = "Total Interest: $" + totalInterest;
	results.style.opacity = "1";
	interestDisplay.style.opacity = "1";
	
	
}

